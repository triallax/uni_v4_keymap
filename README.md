# Uni v4 QMK keymap

Keymap for my Uni 4 stenotype. Same as https://github.com/qmk/qmk_firmware/tree/master/keyboards/stenokeyboards/the_uni/keymaps/utility_belt, but with a QWERTY layer (mostly to play Celeste :3).

## License

GPL 2.0 or later, see [LICENSE.md](./LICENSE.md).
